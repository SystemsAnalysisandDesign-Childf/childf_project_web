var baseURL = 'http://46.101.212.235:8007/';

var authLogin = 'auth/login/';
var authRegister = 'auth/register/';
var authForgetPass = 'auth/forget_pass/';
var authActive = 'auth/active';

var adopted = 'adoption/all/';
var adopt = 'adoption/adopt/';

var getBranch = 'branch/';
var createBranch = 'branch/create/';
var deleteBranch = 'branch/delete/';
var updateBranch = 'branch/update/';

var getComment = 'comment/contacts/';
var createComment = 'comment/create/';
var deleteComment = 'comment/delete/';

var getHonor = 'honors/';
var getAllHonors = 'honors/all/';
var createHonor = 'honors/create/';
var deleteHonor = 'honors/delete/';
var updateHonor = 'honors/update';
var acceptHonor = 'honors/accept/';
var rejectNewHonor = 'honors/reject/create/';
var rejectEditedHonor = 'honors/reject/update/';

var allNews = 'news/all/';
var newsComments = 'news/comments';
var createNews = 'news/create/';
var deleteNews = 'news/delete/';
var updateNews = 'news/update/';

function make_req(url_path, method, get_param, post_params) {
    var request = new XMLHttpRequest();
    var req_url = baseURL;
    req_url += url_path;
    var req_method = 'POST';

    if (get_param !== null) {
        req_url += get_param;
        req_url += '/';
    }

    if (method === 'get') {
        req_method = 'GET';
        request.open(req_method, req_url);
        request.setRequestHeader("Access-Control-Allow-Origin", "*");
        request.responseType = 'json';
        request.send();
    } else {
        var json_body = form_data_to_json(post_params);
        request.open(req_method, req_url);
        request.setRequestHeader("Access-Control-Allow-Origin", "localhost");
        request.setRequestHeader("Content-type", "application/json");
        request.responseType = 'json';
        request.send(json_body);
    }
}

function form_data_to_json(post_params) {
    var json_string = '{';
    var key = '';
    var params = post_params.split('&');
    for (var i = 0; i < params.length; ++i) {
        var param = params[i];
        param = param.split('=');
        key = '"' + param[0] + '"';

        if (param[0] === 'person_type') {
            value = parseInt(param[1]);
        } else {
            value = '"' + param[1] + '"';
        }

        json_string += key;
        json_string += ':';
        json_string += value;

        if (i !== (params.length - 1)) {
            json_string += ', ';
        } else {
            json_string += '}';
        }
    }

    return json_string;
}

function register_func() {
    var first_name = document.getElementById('name').value;
    var last_name = document.getElementById('lname').value;
    var username = document.getElementById('profId').value;
    var phone = document.getElementById('phoneNum').value;
    var email = document.getElementById('mail').value;
    var password = document.getElementById('pass').value;
    var person_type = document.getElementById('profession').value;

    var post_params = 'username=' + String(username) + '&' + 'password=' + String(password) + '&' + 'first_name=' +
        String(first_name) + '&' + 'last_name=' + String(last_name) + '&' + 'email=' + String(email) + '&' +
        'phone=' + String(phone) + '&' + 'person_type=' + String(person_type);

    make_req(authRegister, 'POST', null, post_params);
    window.location.replace("login.html");
}

function login_func() {
    var username = document.getElementById('name').value;
    var password = document.getElementById('pass').value;

    var post_params = 'username=' + String(username) + '&' + 'password=' + String(password);
    make_req(authLogin, 'POST', null, post_params);
}

function active_func() {
    // TODO
}

function forget_password() {
    var username = document.getElementById('tel').value;

    var post_params = 'username=' + String(username);
    make_req(authForgetPass, 'POST', null, post_params);

    document.getElementById('code').style.display = 'block';
    document.getElementById('newPass').style.display = 'block';
    document.getElementById('verifyCode').style.display = 'block';
    document.getElementById('sms').style.display = 'block';
}


function get_adopted_func() {
    make_req(adopted, 'GET', null, null);
    // TODO CREATE DOM USING RETURNED JSON
}

function adopt_child_func(id) {
    make_req(adopt, 'GET', id, null);
    window.location.replace("success_message.html");
}

function get_branch_func(city) {
    make_req(getBranch, 'GET', city, null);
    // TODO CREATE DOM USING RETURNED JSON
}

function create_branch_func() {
    var phone = document.getElementById('phone').value;
    var province = document.getElementById('prov').value;
    var city = document.getElementById('city').value;
    var address = document.getElementById('address').value;
    var hours = document.getElementById('hours').value;

    var post_params = 'phone=' + String(phone) + '&' + 'province=' + String(province) + '&' + 'city=' + String(city) +
        '&' + 'address=' + String(address) + '&' + 'hours_of_availability=' + String(hours);
    make_req(createBranch, 'POST', null, post_params);
    window.location.replace("success_message.html");
}

function delete_branch_func(id) {
    make_req(deleteBranch, 'GET', id, null);
    window.location.replace("success_message.html");
}

function update_branch_func() {
    // TODO
}

function get_comments_func() {
    make_req(getComment, 'GET', null, null);
    // TODO CREATE DOM USING RETURNED JSON
}

function create_comment_func() {
    var author_name = document.getElementById('info-input-name').value;
    var mail_address = document.getElementById('info-input-mail').value;
    var title = document.getElementById('info-input-subject').value;
    var text = document.getElementById('info-input-text').value;
    var creation_datetime = date();
    var last_update_datetime = date();

    var post_params = 'author_name=' + String(author_name) + '&' + 'email=' + String(mail_address) + '&' +
        'title=' + String(title) + '&' + 'text=' + String(text) + '&' +
        'creation_datetime=' + String(creation_datetime) + '&' + 'last_update_datetime=' + String(last_update_datetime);

    make_req(createComment, 'POST', null, post_params);
    alert('نظر شما با موفقیت ثبت شد');
}

function delete_comment_func(id) {
    make_req(deleteComment, 'GET', id, null);
    window.location.replace("success_message.html");
}

function get_honor_func(id) {
    make_req(getHonor, 'GET', id, null);
    // TODO CREATE DOM USING RETURNED JSON
}

function get_all_honors_func(id) {
    make_req(getAllHonors, 'GET', id, null);
    // TODO CREATE DOM USING RETURNED JSON
}

function create_honor_func() {
    var title = document.getElementById('title').value;
    var description = document.getElementById('desc').value;
    var madadju_id = document.getElementById('madadju_id').value;

    var post_params = 'madadju_id=' + String(madadju_id) + '&' + 'title=' + String(title) + '&' + 'description=' + String(description);
    make_req(createHonor, 'POST', null, post_params);
    window.location.replace("success_message.html");
}

function delete_honor(id) {
    make_req(deleteHonor, 'GET', id, null);
    window.location.replace("success_message.html");
}

function update_honor_func() {
    var honor_id = document.getElementById('honor_id').value;
    var title = document.getElementById('title').value;
    var description = document.getElementById('desc').value;

    var post_params = 'id=' + String(id) + '&' + 'title=' + String(title) + '&' + 'description=' + String(description);
    make_req(updateHonor, 'POST', null, post_params);
    window.location.replace("success_message.html");
}

function reject_new_honor_func(id) {
    make_req(rejectNewHonor, 'GET', id, null);
    window.location.replace("success_message.html");
}

function reject_edited_honor_func(id) {
    make_req(rejectEditedHonor, 'GET', id, null);
    window.location.replace("success_message.html");
}

function accept_honor_func(id) {
    make_req(acceptHonor, 'GET', id, null);
    window.location.replace("success_message.html");
}

function get_all_news_func() {
    make_req(allNews, 'GET', null, null);
    // TODO CREATE DOM USING RETURNED JSON
}

function get_news_comments_func(id) {
    make_req(newsComments, 'GET', id, null);
    // TODO CREATE DOM USING RETURNED JSON
}

function create_news_func() {
    var title = document.getElementById('title').value;
    var text = document.getElementById('desc').value;

    var post_params = 'title=' + String(title) + '&' + 'text=' + String(text);
    make_req(createNews, 'POST', null, post_params);
    window.location.replace("success_message.html");
}

function delete_news_func(id) {
    make_req(deleteNews, 'GET', id, null);
    window.location.replace("success_message.html");
}

function update_news_func() {
    var title = document.getElementById('title').value;
    var text = document.getElementById('desc').value;
    var news_id = document.getElementById('news_id').value;

    var post_params = 'news_id=' + String(news_id) + '&' + 'title=' + String(title) + '&' + 'text=' + String(text);
    make_req(updateNews, 'POST', null, post_params);
    window.location.replace("success_message.html");
}



