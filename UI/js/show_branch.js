/**
 * Created by mehran on 12/6/17.
 */

var data_received;
var data_total = {};
var first_shown = false;
var indexes = {'hospital': 0, 'pharmacy': 0, 'office': 0};
var progress;
var data_state = {'hospital': false, 'pharmacy': false, 'office': false};
$(document).ready(function () {


    var intObj = {
        template: 3,
        parent: '#progress-con',
    };
    progress = new Mprogress(intObj);


    // $('#options-btn').click(function () {
    //     var target = $('#options');
    //     var state = target.css('display');
    //     if (state == 'none') {
    //         target.fadeIn(500);
    //         $(this).addClass('option-rotate')
    //     }
    //     else {
    //         target.fadeOut(500);
    //         $(this).removeClass('option-rotate')
    //     }
    // });

    // $('#near-me').click(function () {
    //     if ($.cookie('logged_in') == 1)
    //         window.location.assign(domain_url + '/health_centers/nearme');
    //     else {
    //         $.Toast('ابتدا به حساب خود وارد شوید', {'duration': 2000, 'class': 'alert'});
    //         $('#sign-modal').modal('toggle');
    //     }
    //
    // });

    // $('#hospital-tab').click(function () {
        // $(this).attr('active', 'true');
        // $('#pharmacy-items , #office-items').hide();
        // $('#hospital-items').delay(2).fadeIn(500);
        // $('#pharmacy-tab , #office-tab').attr('active', 'false');
        // $('.office-filter').fadeOut(500);
    // });

    // $('#pharmacy-tab').click(function () {
    //     $(this).attr('active', 'true');
    //     $('#hospital-items , #office-items').hide();
    //     $('#pharmacy-items').delay(2).fadeIn(500);
    //     $('#hospital-tab , #office-tab').attr('active', 'false');
    //     $('.office-filter').fadeOut(500);
    // });
    //
    // $('#office-tab').click(function () {
    //     $(this).attr('active', 'true');
    //     $('#pharmacy-items , #hospital-items').hide();
    //     $('#office-items').delay(2).fadeIn(500);
    //     $('#pharmacy-tab , #hospital-tab').attr('active', 'false');
    //     $('.office-filter').fadeIn(500);
    // });


    // $('#prov').on('change', function () {
    //     var index = $(this)[0].selectedIndex;
    //     fill_city(index, '');
    // });
    //
    // $('#city').on('change', function () {
    //     var prov_text = $("#prov option:selected").text();
    //     var city_text = $("#city option:selected").text();
    //     var url = domain_url + '/health_centers/province/' + prov_text + '/city/' + city_text;
    //     window.location.assign(url);
    // });
    //
    //
    // $('#office-type').on('change', function () {
    //     var category = $("#office-type option:selected").text();
    //     indexes['office'] = 0;
    //     load_office(category, true);
    // });
    //
    //
    // $(window).scroll(function (evenet) {
    //     move_sidebar();
    // });
    // $(window).resize(function () {
    //     var width = $(window).width() + 15;
    //     if (width >= 1050) {
    //         move_sidebar();
    //     }
    //     else {
    //         $('#options').css('top', 'unset');
    //         $('#options').css('bottom ', '90px');
    //     }
    // });

    load_province_city();
    // load_data('hospital');
    // load_data('pharmacy');
    // get_category();
    // pagination();

});


function load_province_city() {


    $.ajax({
        url: domain_url + "/api/v1/health_centers/province_city/",
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Negotiate");
        },
        type: "GET",
        crossDomain: true,
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (data) {
            data_received = data;
            var option = '';
            var province_index;
            for (var obj in data) {
                option = '<option>' + data[obj]['name'] + '</option>';
                $('#prov').append(option);
                if (data[obj]['name'] == province)
                    province_index = obj;

            }
            $('#prov option')[province_index].selected = true;
            fill_city(province_index, city);


        },
        error: function (jXHR, textStatus, errorThrown) {
        }
    });

}

function fill_city(index, name) {
    var temp = data_received[index]['cities'];
    var content = '<option hidden>شهر</option>';
    var city_index = -1;
    for (var i in temp) {
        content += '<option>' + temp[i] + '</option>';
        if (temp[i] == name)
            city_index = parseInt(i)+1;
    }
    $('#city').html(content);
    if (city_index != -1)
        $('#city option')[city_index].selected = true;
}

// function load_data(center) {
//
//     progress.start();
//     var URL = domain_url + "/api/v1/health_centers/" + center + "/?province=" + province + "&city=" + city;
//     $.ajax({
//         url: URL,
//         beforeSend: function (request) {
//             request.setRequestHeader("Authorization", "Negotiate");
//         },
//         type: "GET",
//         crossDomain: true,
//         contentType: "application/json;charset=utf-8",
//         datatype: "json",
//         success: function (data) {
//             data_state[center] = true;
//             if (all_received())
//                 progress.end();
//             data_total[center] = data;
//             show_data(center, true);
//         },
//         error: function (jXHR, textStatus, errorThrown) {
//
//         }
//     });
//
// }

// function get_category(center) {
//     var URL = domain_url + '/api/v1/physician/profession_category/';
//     var category;
//     $.ajax({
//         url: URL,
//         beforeSend: function (request) {
//             request.setRequestHeader("Authorization", "Negotiate");
//         },
//         type: "GET",
//         crossDomain: true,
//         contentType: "application/json;charset=utf-8",
//         datatype: "json",
//         success: function (data) {
//             category = data;
//             var content = '';
//             for (var i in data) {
//                 content += '<option>' + data[i]['name'] + '</option>';
//             }
//             $('#office-type').html(content);
//             load_office(data[0]['name'], true);
//
//         },
//         error: function (jXHR, textStatus, errorThrown) {
//         }
//     });
//     return category;
// }


// function load_office(category, clear) {
//     progress.start();
//     var URL = url = domain_url + '/api/v1/health_centers/physician/?province=' + province + '&city=' + city + '&category=' + category;
//     $.ajax({
//         url: URL,
//         beforeSend: function (request) {
//             request.setRequestHeader("Authorization", "Negotiate");
//         },
//         type: "GET",
//         crossDomain: true,
//         contentType: "application/json;charset=utf-8",
//         datatype: "json",
//         success: function (data) {
//             data_state['office'] = true;
//             if (all_received())
//                 progress.end();
//             data_total['office'] = data;
//             show_data('office', clear);
//         },
//         error: function (jXHR, textStatus, errorThrown) {
//             console.log(errorThrown)
//         }
//     });
// }

// function show_data(type, clear) {
//
//     var step = 20;
//
//     var name = 'name';
//     if (type == 'pharmacy')
//         name = 'special_name';
//
//
//     if (type == 'hospital')
//         first_shown = true;
//
//     var len = data_total[type].length;
//     var current_idx = indexes[type];
//
//     if (clear) {
//         $('#' + type + '-items').html('');
//     }
//
//
//     for (var i = 0; ( i < step && current_idx + i < len); i++) {
//
//         var url = data_total[type][current_idx + i]['url'];
//         var temp = url.split('/');
//         var url = temp[temp.length - 3] + '/' + temp[temp.length - 2];
//
//         var content = '<div url="' + url + '" class="row-v health-item transition1 ' + type + '" onclick="item_click(this)">';
//         content += '<p class="subtitle">' + data_total[type][current_idx + i][name] + '</p>';
//         content += ' <div class="' + type + '-l"></div>';
//
//         if (type != 'office')
//             content += '<p class="body-text">' + data_total[type][current_idx + i]['address'] + '</p>';
//         else
//             for (var j in data_total[type][current_idx + i]['profession'])
//                 content += '<p class="body-text">' + data_total[type][current_idx + i]['profession'][j] + '</p>';
//
//         content += '</div>';
//         $('#' + type + '-items').removeClass('health-items-bkg');
//         $('#' + type + '-items').append(content);
//
//     }
//
//     if (len == 0) {
//         $('#' + type + '-items').html('<div class="body-text h-center" id="empty-tab-message">موردی پیدا نشد</div>')
//         $('#' + type + '-items').addClass('health-items-bkg');
//     }
//
//     indexes[type] += step;
//
// }


// function item_click(e) {
//     var url = $(e).attr('url');
//     var name = $(e).children('.subtitle').text();
//     window.location.assign(domain_url + '/health_centers/details/' + url + '/' + name);
// }


// function pagination() {
//     $(window).scroll(function () {
//         var dis = $('#footer').offset().top - ( $(window).scrollTop() + $(window).height()  );
//         if (dis < 200 && first_shown) {
//             var target;
//             if ($('#hospital-tab').attr('active') == 'true')
//                 target = 'hospital';
//             else if ($('#pharmacy-tab').attr('active') == 'true')
//                 target = 'pharmacy';
//             else if ($('#office-tab').attr('active') == 'true')
//                 target = 'office';
//             show_data(target, false);
//         }
//     });
// }

// function move_sidebar() {
//     var width = $(window).width() + 15;
//     var pos_top = 120;
//     var height = $('#options').height();
//     var margin = 50;
//     var dis = $(window).scrollTop() - $('#footer').offset().top + margin + pos_top + height; //numbers area top and height of sidebar
//     if (width >= 1050) {
//         $('#options').css('bottom ', 'unset');
//         if (dis > 0) {
//             var top = pos_top - dis;
//             $('#options').css('top', top + 'px');
//         }
//         else {
//             $('#options').css('top', pos_top + 'px');
//         }
//     }
//
// }
//
// function all_received() {
//
//     return data_state['office'] & data_state['pharmacy'] & data_state['hospital'];
// }