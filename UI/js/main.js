/**
 * Created by mehran on 10/26/17.
 */

// ------------------------------------------
$(document).ready(function () {

    //handle place holder problem for input type number
    $('input[type=number]').attr('type', 'text').attr('state', 'init').attr('temp', '');

    $('input[state=init]').focusin(function (e) {
        $(this).attr('type', 'number');
        //avoid none nun input in num input (fore firefox , chrome is ok by default)
        $('input[type=number]').keypress(function (evt) {
            if ((evt.which < 48 || evt.which > 57) && evt.which != 8 && evt.which != 0)
                evt.preventDefault();
        });
    });

    $('input[state=init]').click(function (event) {
        event.stopPropagation()
    });

    $(window).click(function () {
        $('input[state=init]').attr('type', 'text');
    });

    // ----------------------------

    window.onclick = function (event) {
        if (event.target == medicalVideoModal) {
            document.getElementById('video1').pause();
            $('#video1').fadeOut(500);
            document.getElementById('video2').pause();
            $('#video2').fadeOut(500);
            $('#medicalVideoModal').fadeOut(500);
        }
    };


    $('#firstVideoPlay').on('click', function (event) {
        event.preventDefault();
        var src = static_url + '/video/roode.mp4';
        $('#video1').fadeIn(500);
        $('#medicalVideoModal').fadeIn(500);
        $('#roode-src').attr('src', src);
        document.getElementById("video1").load();
        document.getElementById('video1').play();

    });

    $('#secondVideoPlay').on('click', function (event) {
        event.preventDefault();
        var src = static_url + '/video/migren.mp4';
        $('#video2').fadeIn(500);
        $('#medicalVideoModal').fadeIn(500);
        $('#migren-src').attr('src', src);
        document.getElementById("video2").load();
        document.getElementById('video2').play();

    });


    // --------------------------------------------

    $('#start-vv').click(function () {

        if ($.cookie('logged_in') == 1) {

            var vv_url = domain_url + '/symptom_checker/';
            window.location.assign(vv_url)

        }
        else {
            $.Toast('ابتدا به حساب خود وارد شوید', {'duration': 2000, 'class': ''});
            $('#sign-modal').modal('toggle')

        }
    });


    $('.scrollDown').click(function () {
        $('body , html').animate({scrollTop: $('.scrollDown').offset().top + 100}, 'slow');

    });


    $('.nearMeButton').click(function () {

        if ($.cookie('logged_in') == 1) {
            window.location.assign(domain_url + '/health_centers/nearme');
        }
        else {
            $.Toast('ابتدا به حساب خود وارد شوید', {'duration': 2000, 'class': ''});
        }
    })


});


function send_app_link() {

    var number;
    var temp = $('#send-link-in').val();
    if (temp == '')
        $.Toast('شماره خود را وارد کنید', {'duration': 2000, 'class': 'alert'});
    else {
        if (temp[0] == '0')
            number = temp.substr(1)
        else
            number = temp;

        api_url = '/api/v1/utility/send_app_link/?phone_number=';
        $.ajax({
            url: domain_url + api_url + number + '&code=98',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Negotiate");
            },
            type: "GET",
            crossDomain: true,
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                $.Toast('لینک دانلود برای شما پیامک شد', {'duration': 2000, 'class': ''});
            },
            error: function (jXHR, textStatus, errorThrown) {
                $.Toast('خطا در ارسال اطلاعات', {'duration': 2000, 'class': 'alert'});
            }
        });
    }

}


function store_number() {

    var number;
    var temp;
    temp = $('#comming-in').val();
    if (temp == '')
        $.Toast('شماره خود را وارد کنید', {'duration': 2000, 'class': 'alert'});
    else {

        if (temp[0] == '0')
            number = temp.substr(1)
        else
            number = temp;
        $.ajax({
            url: domain_url + '/api/v1/content_pool/coming_soon/',
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Negotiate");
                request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
            },
            type: "POST",
            crossDomain: true,
            data: jQuery.param({phone: number, code: '98'}),
            success: function (data) {
                $.Toast('به زودی خبر های خوبی به شما می دهیم :)', {'duration': 2000, 'class': ''});

            },
            error: function (jXHR, textStatus, errorThrown) {
                var data = jXHR['responseJSON']['phone'][0];
                if (data.indexOf('exist') > 0)
                    $.Toast('شماره ی شماقبلا ثبت شده است', {'duration': 2000, 'class': 'alert'});
                else
                    $.Toast('خطا در ارسال اطلاعات', {'duration': 2000, 'class': 'alert'});
            }
        });
    }

}

