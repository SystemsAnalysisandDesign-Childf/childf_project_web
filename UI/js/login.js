/**
 * Created by parisa on 6/1/17.
 */

var request_url = global_url + '/api';
var request_url_physician = global_url + '/physician';
var telNum;


$(window).ready(function () {

    // $('#pass').val($.cookie('pass')); //todo add remember me for login
    // $('#phone_number').val($.cookie('user'));
    $('#name').fadeIn(1000);
    $('#pass').fadeIn(1000);
    $('#loginButton').fadeIn(1000);
    $('#remember').fadeIn(1000);
    $('#notRegistered').fadeIn(1000);
    $('#forgetPass').fadeIn(1000);

});


$('#name').focusin(function () {


    $(this).css("border", "white solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/user.png)");

});

$('#name').focusout(function () {


    $(this).css("border", "#93B6C7 solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/user_50.png)");
});


$('#pass').focusin(function () {


    $(this).css("border", "white solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/pass.png)");

});

$('#pass').focusout(function () {


    $(this).css("border", "#93B6C7 solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/pass_50.png)");

});

$("#loginButton").on('click', function (event) {
    event.preventDefault();
    var userName = document.getElementById('name').value;
    var pass = document.getElementById('pass').value;


    if (userName === "") {
        $('#name').css('border', '#FC6F6F solid 2px');
    }

    else if (userName.substring(0, 2) != "09") {
        $('#name').css('border', '#FC6F6F solid 2px');
    }
    else if (userName.length != "11") {
        $('#name').css('border', '#FC6F6F solid 2px');
    }
    else if (pass === "") {
        $('#pass').css('border', '#FC6F6F solid 2px');
    }
    else {
        $.ajax({

            url: request_url + "/v1/physician/login/",
            type: "POST",
            datatype: "json",
            beforeSend: function (request) {
                request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
            },
            data: {
                phone_number: userName,
                password: pass
            },
            success: function (data) {


                if ($('#checkBox').on('click') == true) {


                    var pass = document.getElementById('pass').value;
                    var phoneNumber = document.getElementById('name').value;

                    $.cookie('pass', pass, {expires: 365, path: 'physician/login'});
                    $.cookie('user', userName, {expires: 365, path: 'physician/login'});

                }
                ;


                $('#notPhysician').hide();


                $.cookie('physician_auth', "TOKEN " + data['token'], {expires: 365, path: '/'});
                $.cookie('phone_number', userName, {expires: 365, path: '/'});

                if (data['darmaneh_verified'] == false) {

                    $('#notPhysician').hide();
                    $('#wrongPass').hide();
                    $('#name').hide();
                    $('#pass').hide();
                    $('.rememberMe').hide();
                    $('#checkBox').hide();
                    $('#loginButton').hide();
                    $('#notRegistered').hide();
                    $('#forgetPass').hide();
                    $('#notVerified').fadeIn(1000);
                }
                else {


                    window.location.href = request_url_physician + '/panel/';
                    //request_url_physician +
                }

            },
            error: function (jXHR, textStatus, errorThrown) {

                x = JSON.parse(jXHR.responseText);
                if (x.detail == "physician is not verification code") {

                    $('#notPhysician').fadeIn(1000);
                    $('#wrongPass').hide();


                }
                else if (x.detail == "password mismatch") {

                    $('#pass').css('border', '#FC6F6F solid 1px');
                    $('#wrongPass').fadeIn(1000);
                    $('#notPhysician').hide();
                }

                else if (x.detail == "physician does not exist.") {
                    $('#notPhysician').fadeIn(1000);
                    $('#wrongPass').hide();

                }

            }

        });
    }


});

$('#notRegistered').on('click', function (event) {
    event.preventDefault();
    window.location.href = global_url + '/physician/signup';
});

$('#forgetPass').on('click', function () {
    $('#name').fadeOut(800);
    $('#pass').fadeOut(800);
    $('#loginButton').fadeOut(800);
    $('#remember').fadeOut(800);
    $('#notRegistered').fadeOut(800);
    $('#forgetPass').fadeOut(800);
    $('#tel').delay(1000).fadeIn(1000);
    $('#verificationCode').delay(1000).fadeIn(1000);
    $('#notPhysician').hide();
    $('#wrongPass').hide();

});

$('#verificationCode').on('click', function (event) {
    event.preventDefault();
    var phone_number = document.getElementById('tel').value;
    $('#tel').fadeOut(1000);
    $('#verificationCode').fadeOut(1000);

    $.ajax({
        url: request_url + "/v1/physician/send_verification/",
        type: "POST",
        beforeSend: function (request) {
            request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
        },
        data: {phone_number: phone_number, protocol: 'sms'},
        success: function (data) {
            telNum = phone_number;
            $('#code').delay(1000).fadeIn(1000);
            $('#newPass').delay(1000).fadeIn(1000);
            $('#verifyCode').delay(1000).fadeIn(1000);
            $('#sms').delay(1000).fadeIn(1000);

        },
        error: function (jXHR, textStatus, errorThrown) {
        }
    });


});

$('#verifyCode').on('click', function (event) {
    event.preventDefault();
    var code = document.getElementById('code').value;
    var pass = document.getElementById('newPass').value;
    var button = document.getElementById('verifyCode');
    button.innerText = " ";
    var spinner = document.createElement('div');
    spinner.className = 'loader';
    button.appendChild(spinner);

    $.ajax({
        url: request_url + "/v1/physician/set_change_pass/",
        type: "POST",
        dataType: 'json',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        beforeSend: function (request) {
            request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
        },
        data: {phone_number: telNum, password: pass, verify_code: code},
        success: function (data) {
            $('#code').hide();
            $('#newPass').hide();
            $('#verifyCode').css('background-color', '#24B337');
            document.getElementById('verifyCode').innerHTML = "";
            $('#verifyCode').css('background-image', "url(" + static_url + "/img/icons/check-mark.png)");
            $('#verifyCode').css('background-position', "50% 50%");
            $('#verifyCode').css("background-repeat", "no-repeat");
            document.getElementById('sms').innerHTML = 'رمز عبور جدید برای شما ساخته شد';
            $('#backLogin').show();

        },
        error: function () {
        }
    })
});

$('#backLogin').on('click', function () {
    window.location.href = request_url_physician + "/login/"
});



