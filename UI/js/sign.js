/**
 * Created by mehran on 10/29/17.
 */
/**
 * Created by mehran on 8/10/17.
 */
var scroll_en = true;
var prefix = 98;
var name_pop;
var lastname_pop;

var sign_sex = '';
var sign_birth;
var sign_height;
var sign_weight;
var current_year;

var log_sign;
var domain_url_header = domain_url;
var progressbar1;
var progressbar2;
var progressbar3;
var progressbar4;
$(document).ready(function () {


    $('#prefix-show-log').val('+' + '98');
    $('#prefix-show-sign').val('+' + '98');


    var intObj = {
        template: 3,
        parent: '#prog1',
    };
    progressbar1 = new Mprogress(intObj);

    var intObj = {
        template: 3,
        parent: '#prog2',
    };
    progressbar2 = new Mprogress(intObj);


    var intObj = {
        template: 3,
        parent: '#prog3',
    };
    progressbar3 = new Mprogress(intObj);


    var intObj = {
        template: 3,
        parent: '#prog4',
    };
    progressbar4 = new Mprogress(intObj);


    var d = new Date();

    var month_miladi = d.getMonth() + 1;
    var day_miladi = d.getDate();

    var year_miladi = d.getFullYear();
    var date_shamsi = gregorian_to_jalali(year_miladi, month_miladi, day_miladi);

    current_year = date_shamsi[0];


    $('#sign-male').click(function () {
        $(this).removeClass('sign-male-d');
        $(this).addClass('sign-male');
        $('#sign-female').removeClass('sign-female');
        $('#sign-female').addClass('sign-female-d');
        sign_sex = 'male';

    });
    $('#sign-female').click(function () {
        $(this).removeClass('sign-female-d');
        $(this).addClass('sign-female');
        $('#sign-male').removeClass('sign-male');
        $('#sign-male').addClass('sign-male-d');
        sign_sex = 'female';

    });


    if ($.cookie("logged_in") === undefined || $.cookie("logged_in") == 2)
        $.cookie("logged_in", 0, {expires: 30, path: '/'});

    if ($.cookie("logged_in") == 0) {

        $("#sign-btn").html("ورود");
    }
    else {
        prepare_sign_final();
        $('#log-in').hide();
        $('#sign-final').show();
    }

    $('#sign-btn').click(function () {

        if ($('.modal').css('display') == 'none') {

            $('#new-pass .sign-header h2').text('انتخاب رمز عبور');
            $('#new-pass .sign-content .sign-step').show();
            $('#new-pass .sign-content h2.bf-text').css('margin-top', '0');
            sign_sex = '';
            $('#sign-modal input:not(.avoid-clear)').val('');
            $('#sign-up').hide();
            $('#new-pass').hide();
            $('#sign-info1').hide();
            $('#sign-info2').hide();
            $('#sign-up').hide();
            $('#sign-final').hide()
            $('#log-in').hide()
            if ($.cookie("logged_in") == '1') {
                prepare_sign_final()
                $('#sign-final').show()
            }
            else {
                $('#log-in').show();
                setTimeout(function () {
                    $('#log-num').focus();
                }, 600)

            }

        }
        $('#sign-modal').modal('toggle');

    });

    $('.sign-modal-close').click(function () {
        $('#sign-modal').modal('toggle');
    });

    $('#sign-modal').on('hidden', function () {
    });

    $('#sign-modal input').focusout(function () {
        $(this).siblings('.bar-input-g').removeClass('bar-alert');
    });


    prefix_init();

});


function prefix_init() {

    $(".prefix-num-log").intlTelInput();
    $(".prefix-num-sign").intlTelInput();

    prefix = 98;

    $(".prefix-num-log").on("countrychange", function (e, countryData) {
        // do something with countryData
        $('#prefix-show-log').val('+' + countryData.dialCode)
        prefix = countryData.dialCode;

    });


    $(".prefix-num-sign").on("countrychange", function (e, countryData) {
        // do something with countryData
        $('#prefix-show-sign').val('+' + countryData.dialCode)
        prefix = countryData.dialCode;

    });


}


function pop_sign_click() {

    setTimeout(function () {
        // prefix_init();
        $('#sign-up').fadeIn(500);

    }, 500);
    $('#log-in').fadeOut(500)

}


function log_num_admit_click() {
    var temp = $('#log-num').val();
    var password = $('#log-pass').val();

    if (temp == '') {
        fade_input_pop('#log-num')
    }
    else if (password == '') {
        fade_input_pop('#log-pass')
    }
    else {
        var number;
        if (temp[0] == '0')
            number = temp.substr(1)
        else
            number = temp;
        authorization_pop(number, prefix, password, 'check_pass', 'login')
    }
}

function sign_num_admit_click() {
    var temp = $('#sign-num-input').val();
    var number;
    if (temp[0] == '0')
        number = temp.substr(1)
    else
        number = temp;

    if (number == '')
        fade_input_pop('#sign-num-input')
    else {
        $('#sign-num-admit').append('<span id="rotator-gly" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
        authorization_pop(number, prefix, '', 'sign_up')
    }

}


function forgot_pass_link() {
    var number = $('#log-num').val();
    if (number == '')
        fade_input_pop('#log-num')
    else {
        authorization_pop(number, prefix, 'pass', 'forgot', 'login')
    }
}

function set_pass_click() {
    var code = $('#code-input').val()
    var new_pass = $('#new-pass-input').val();
    var repeat_pass = $('#repeat-pass-input').val();
    if (code == '')
        fade_input_pop('#code-input')
    else if (new_pass == '' || new_pass.length < 5)
        fade_input_pop('#new-pass-input')
    else if (repeat_pass == '')
        fade_input_pop('#repeat-pass-input')
    else if (new_pass != repeat_pass) {
        $.Toast('رمز عبور و تکرار آن با هم یکسان نیستند', {'duration': 2000, 'class': 'alert'});
        setTimeout(function () {
            $('#new-pass-input').val('');
            $('#repeat-pass-input').val('');
        }, 1000)
    }
    else {
        $('#new-pass-admit').append('<span id="rotator-gly" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>');
        set_new_pass_pop(new_pass, code)
    }
}


function info1_click() {
    name_pop = $('#sign-name').val()
    lastname_pop = $('#sign-lastname').val()
    if (name_pop == '') {
        fade_input_pop('#sign-name')
    }
    else if (lastname_pop == '') {
        fade_input_pop('#sign-lastname')
    }
    else {
        $('#sign-info1').fadeOut(500)
        setTimeout(function () {
            $('#sign-info2').fadeIn(500)
        }, 500)
    }
}


function info2_click() {

    sign_birth = $('#sign-age').val();
    sign_height = $('#sign-height').val();
    sign_weight = $('#sign-weight').val();

    if (sign_sex == '') {
        $.Toast('لطفا جنسیت خود را انتخاب کنید', {'duration': 2000, 'class': 'alert'});
    }
    else if (sign_birth == '') {
        fade_input_pop('#sign-age')
    }
    else if (sign_height == '') {
        fade_input_pop('#sign-height')
    }
    else if (sign_weight == '') {
        fade_input_pop('#sign-weight')
    }
    else {
        sign_birth = current_year - parseInt(sign_birth);
        store_user_info_pop();
    }
}


function voice_call_click() {
    resend_code_pop('voice', '#pop-note')
}


//authorization
function authorization_pop(number, prefix, password, mode, log) {

    number = persian_to_latin(number);
    progressbar1.start();
    progressbar2.start();
    progressbar3.start();
    progressbar4.start();
    $.ajax({
        url: domain_url_header + "/api/v3/user/patient/check_registered/",
        beforeSend: function (request) {
            request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
        },
        type: "POST",
        data: jQuery.param({phone_number: number, code: prefix}),
        success: function (data) {
            if (data['detail'] = 'success') {
                $('#rotator-gly').remove();
                $.cookie("phone_number", number, {expires: 30, path: '/'});
                $.cookie("prefix", prefix, {expires: 30, path: '/'})
                if (data['status'] == 'registered') {
                    if (log == 'login') {
                        if (mode == 'check_pass')
                            check_pass_pop(password);
                        else {
                            log_sign = 'log';
                            forgot_pass_pop();
                        }
                    }
                    else {
                        progressbar1.end();
                        progressbar2.end();
                        progressbar3.end();
                        progressbar4.end();
                        $.Toast('شما قبلا ثبت نام کرده اید', {'duration': 2000, 'class': ''});
                        $('#log-num').val(number);

                        setTimeout(function () {
                            // prefix_init();
                            $('#log-in').fadeIn(500);
                        }, 2000)
                        setTimeout(function () {
                            $('#sign-up').fadeOut(500)
                        }, 1500)
                    }
                }
                else {
                    if (log == 'login') {
                        progressbar1.end();
                        progressbar2.end();
                        progressbar3.end();
                        progressbar4.end();
                        $.Toast('شما قبلا ثبت نام نکرده اید', {'duration': 2000, 'class': 'alert'});
                        setTimeout(function () {
                            $('#sign-num-input').val(number);
                            // prefix_init();
                            $('#sign-up').fadeIn(500);
                        }, 500);

                        $('#log-in').fadeOut(500)

                    }
                    else {
                        log_sign = 'sign';
                        forgot_pass_pop();
                    }
                }

            }

        },
        error: function (jXHR, textStatus, errorThrown) {
            $('#rotator-gly').remove();
            console.log(errorThrown);
            progressbar1.end();
            progressbar2.end();
            progressbar3.end();
            progressbar4.end();
            $.Toast('خطایی رخ داده است ، شماره نامعتبر', {'duration': 2000, 'class': 'alert'});
        }
    });
}


//checks entered password
function check_pass_pop(password) {

    $.ajax({
        url: domain_url_header + "/api/v3/user/patient/login/",
        beforeSend: function (request) {
            request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
        },
        type: "POST",
        crossDomain: true,
        data: jQuery.param({
            code: $.cookie("prefix"),
            phone_number: $.cookie("phone_number"),
            password: password
        }),
        success: function (data) {

            $.cookie("token", data['token'], {expires: 30, path: '/'});
            $.cookie("logged_in", 1, {expires: 30, path: '/'});

            if (data['detail'] = 'success') {

                get_user_info_pop();
            }

        },
        error: function (jXHR, textStatus, errorThrown) {
            progressbar1.end();
            progressbar2.end();
            progressbar3.end();
            progressbar4.end();
            $.Toast('رمز عبور اشتباه است', {'duration': 2000, 'class': 'alert'});

        }
    });


}


function forgot_pass_pop() {

    if (log_sign == 'log') {
        $('#new-pass .sign-header h2').text('فراموشی رمز عبور');
        $('#new-pass .sign-content .sign-step').hide();
        $('#new-pass .sign-content h2.bf-text').css('margin-top', '40px')
    }


    $.ajax({
        url: domain_url_header + "/api/v3/user/patient/send_verification/",
        beforeSend: function (request) {
            request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
        },
        type: "POST",
        crossDomain: true,
        data: jQuery.param({phone_number: $.cookie("phone_number"), code: $.cookie("prefix"), protocol: 'sms'}),
        success: function (data) {
            progressbar1.end();
            progressbar2.end();
            progressbar3.end();
            progressbar4.end();
            if (log_sign == 'log') {
                $('#log-in').fadeOut(500);
                setTimeout(
                    function () {
                        $('#new-pass').fadeIn(500);
                    }, 500);
            }
            else {
                $('#sign-up').fadeOut(500);
                setTimeout(
                    function () {
                        $('#new-pass').fadeIn(500);
                    }, 500);
            }

        },
        error: function (jXHR, textStatus, errorThrown) {
            console.log(errorThrown);
            progressbar1.end();
            progressbar2.end();
            progressbar3.end();
            progressbar4.end();
            $.Toast('ارسال کد با خطا مواجه شد لطفا دوباره تلاش کنید', {'duration': 2000, 'class': 'alert'});
        }
    });
}


function set_new_pass_pop(password, code, log) {

    progressbar1.start();
    progressbar2.start();
    progressbar3.start();
    progressbar4.start();
    $.ajax({
        url: domain_url_header + "/api/v3/user/patient/set_change_pass/",
        beforeSend: function (request) {
            request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
        },
        type: "POST",
        crossDomain: true,
        data: jQuery.param({
            code: $.cookie("prefix"),
            phone_number: $.cookie("phone_number"),
            password: password,
            verify_code: code,
        }),
        success: function (data) {
            $('#rotator-gly').remove();
            $.cookie("token", data['token'], {expires: 30, path: '/'});
            $.cookie("logged_in", 1, {expires: 30, path: '/'});

            if (data['detail'] == 'success') {


                if (log_sign == 'sign')
                // store_user_info_pop();
                {
                    progressbar1.end();
                    progressbar2.end();
                    progressbar3.end();
                    progressbar4.end();
                    $('#new-pass').fadeOut(500);
                    setTimeout(
                        function () {
                            $('#sign-info1').fadeIn(500);

                        }, 500);
                }

                else {
                    get_user_info_pop();
                    $('#new-pass').fadeOut(500);
                    setTimeout(
                        function () {
                            $('#sign-final').fadeIn(500);

                        }, 500);
                    // if (page_type == 'pool')
                    //     location.reload();
                }
            }
        },
        error: function (jXHR, textStatus, errorThrown) {
            progressbar1.end();
            progressbar2.end();
            progressbar3.end();
            progressbar4.end();
            $('#rotator-gly').remove();
            var data = jXHR['responseJSON'];
            if (data['code'] == 6) {
                fade_alert_pop("کد وارد شده نامعتبر است", "red", "#pop-note")
            }
            else if (data['code'] == 9) {
                fade_alert_pop("کد وارد شده اشتباه است", "red", "#pop-note")
            }
            else if (data['code'] == 8) {
                fade_alert_pop("کد وارد شده منقضی شده است", "red", "#pop-note")
                fade_alert_pop("کد دوباره برای شما ارسال شد", "red", "#pop-note")
                resend_code_pop("sms", "#pop-note");
            }
            else {
                fade_alert_pop("ارسال با خطا مواجه شد", "red", "#pop-note")
            }

        }
    });


}


// in case of code expiration use this to resend code to user
function resend_code_pop(protocol, id) {
    $.ajax({
        url: domain_url_header + "/api/v3/user/patient/send_verification/",
        beforeSend: function (request) {
            request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
        },
        type: "POST",
        crossDomain: true,
        data: jQuery.param({phone_number: $.cookie("phone_number"), code: $.cookie("prefix"), protocol: protocol}),
        success: function (data) {
            var temp = "کد برای شما ارسال شد";
            if (protocol == "voice")
                temp = "تا حداکثر دو دقیقه ی دیگر با شما تماس خواهیم گرفت";
            fade_alert_pop(temp, "gray", id);
            return true;

        },
        error: function (jXHR, textStatus, errorThrown) {
            console.log(errorThrown);
            fade_alert_pop("ارسال با خطا مواجه شد لطفا دوباره تلاش کنید", "red", id);
            return false;
        }
    });
}


//store user info
function store_user_info_pop() {
    progressbar1.start();
    progressbar2.start();
    progressbar3.start();
    progressbar4.start();
    $.ajax({
        url: domain_url_header + "/api/v1/user/patient/info/",
        beforeSend: function (request) {
            request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
            request.setRequestHeader("Authorization", "TOKEN " + $.cookie("token"));
            request.withCredentials = true;

        },
        type: "PUT",
        contentType: "application/x-www-form-urlencoded",
        datatype: "json",
        data: jQuery.param({
            first_name: name_pop,
            last_name: lastname_pop,
            sex: sign_sex,
            birth_year: sign_birth,
            height: sign_height,
            weight: sign_weight
        }),
        success: function (data) {
            progressbar1.end();
            progressbar2.end();
            progressbar3.end();
            progressbar4.end();
            $.cookie("user_birth_year", sign_birth, {expires: 30, path: '/'});
            $.cookie("user_height", parseInt(sign_height), {expires: 30, path: '/'});
            $.cookie("user_weight", parseInt(sign_weight), {expires: 30, path: '/'});
            $.cookie("first_name", name_pop, {expires: 30, path: '/'});
            $.cookie("last_name", lastname_pop, {expires: 30, path: '/'});
            $.cookie("user_sex", sign_sex, {expires: 30, path: '/'});
            $('#sign-info2').fadeOut(500);
            setTimeout(
                function () {
                    prepare_sign_final();
                    $('#sign-final').fadeIn(500);

                }, 500);

            // if (page_type == 'pool')
            //     location.reload();

        },
        error: function (jXHR, textStatus, errorThrown) {
            console.log(errorThrown);
            progressbar1.end();
            progressbar2.end();
            progressbar3.end();
            progressbar4.end();
            fade_alert_pop('ارسال اطلاعات با خطا مواجه شد', 'red', '#pop-note')
        }
    });

}


function get_user_info_pop() {

    $.ajax({
        url: domain_url_header + "/api/v1/user/patient/info/",
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "TOKEN " + $.cookie("token"));
            request.withCredentials = true;
        },
        type: "GET",
        contentType: "application/x-www-form-urlencoded",
        datatype: "json",
        success: function (data) {
            progressbar1.end();
            progressbar2.end();
            progressbar3.end();
            progressbar4.end();
            $.cookie("user_birth_year", data['birth_year'], {expires: 30, path: '/'});
            $.cookie("user_height", parseInt(data['height']), {expires: 30, path: '/'});
            $.cookie("user_weight", parseInt(data['weight']), {expires: 30, path: '/'});
            $.cookie("first_name", data['first_name'], {expires: 30, path: '/'});
            $.cookie("last_name", data['last_name'], {expires: 30, path: '/'});
            $.cookie("user_sex", data['sex'], {expires: 30, path: '/'});
            prepare_sign_final();

            $('#log-in').fadeOut(500);
            setTimeout(
                function () {
                    $('#sign-final').fadeIn(500);
                }, 500);


        },
        error: function (jXHR, textStatus, errorThrown) {
            console.log(errorThrown);
            progressbar1.end();
            progressbar2.end();
            progressbar3.end();
            progressbar4.end();
            $.Toast('دریافت اطلاعات با خطا مواجه شد', {'duration': 2000, 'class': 'alert'});
        }
    });

}

function prepare_sign_final() {
    $("#sign-btn").css("display", "block");
    var name = $.cookie("first_name");
    if (name == '' || name === undefined) {
        name = 'کاربر';
        $('#final-name').text('اطلاعات شما کامل نیست لطفا در پروفایل اطلاعات خود را وارد کنید');
        $('#final-avatar').addClass('sign-male-d');
    }
    else {
        $('#final-avatar').removeClass('sign-male-d');
        $('#final-num').text(latin_to_persian($.cookie("phone_number")));
        $('#final-name').text($.cookie("first_name") + ' ' + $.cookie("last_name"));
        if ($.cookie("user_sex") == "male")
            $('#final-avatar').addClass('sign-male');
        else
            $('#final-avatar').addClass('sign-female')
    }
    $("#sign-btn").html("سلام" + " " + name + " " + "عزیز");
}

function exit_pop_click() {

    $('#sign-modal').modal('toggle');
    setTimeout(function () {
        $('#log-in').show();
        $('#sign-final').hide()
    }, 500);
    $.cookie("logged_in", 0, {path: '/'});
    $.cookie("user_birth_year", "", {path: '/'});
    $.cookie("user_height", 0, {path: '/'});
    $.cookie("user_weight", 0, {path: '/'});
    $.cookie("first_name", "", {path: '/'});
    $.cookie("last_name", "", {path: '/'});
    $.cookie("user_sex", "", {path: '/'});
    $.cookie("token", "", {path: '/'});
    $.cookie("phone_number", 0, {path: '/'});
    $.cookie("prefix", 0, {path: '/'});
    $("#sign-btn").html("ورود");


    if (page_type == 'pool')
        location.reload();


    if (page_type == "profile" || page_type == "condition" || page_type == "visit" || page_type == 'record' || page_type == 'nearme')
        window.location.assign(domain_url_header)


}


function profile_btn_click() {
    // if (page_type != "profile")
    window.location.assign(domain_url_header + '/profile/')
}


function record_btn_click() {
    if (page_type != "record")
        window.location.assign(domain_url_header + '/record/')
}

function bookmarked_btn_click() {
    if (page_type != "condition")
        window.location.assign(domain_url_header + '/condition_detail/')
}


function fade_alert_pop(text, color, id) {
    $.Toast(text, {'duration': 2000, 'class': 'alert'});

}
function fade_input_pop(id) {

    $(id).focus()
    $(id + '+ .bar-input-g').addClass('bar-alert')
}

//validate pass : lenght and containing alphabets
function validate_pass_pop(id, out) {

    if ($(id).val().length < 6 || $(id).val().match(/[a-z]/i) == null) {
        fade_alert_pop("رمز عبور باید حداقل ۶ کاراکتر و شامل حروف و اعداد باشد", "red", out)
        setTimeout(function () {
            $(id).val("")
            $(id).val("")
        }, 1000)
        return false;
    }
    else {
        return true;
    }
}

function validateEmail_pop(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}


function persian_to_latin(num) {

    //num must be string
    var persian_num = "۰۱۲۳۴۵۶۷۸۹";
    var per = false;
    var out = "";
    var lim = num.length;
    for (var i = 0; i < lim; i++) {
        var idx = persian_num.indexOf(num[i]);
        if (idx != -1) {
            per = true;
            out = out.concat(idx);
        }
    }
    if (per)
        return out;
    else
        return num;
}

function latin_to_persian(num) {

    //num must be string
    var persian_num = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
    var out = "";
    var lim = num.length;
    for (var i = 0; i < lim; i++) {
        out = out.concat(persian_num[parseInt(num[i])])
    }
    return out
}


function gregorian_to_jalali(gy, gm, gd) {
    g_d_m = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
    if (gy > 1600) {
        jy = 979;
        gy -= 1600;
    } else {
        jy = 0;
        gy -= 621;
    }
    gy2 = (gm > 2) ? (gy + 1) : gy;
    days = (365 * gy) + (parseInt((gy2 + 3) / 4)) - (parseInt((gy2 + 99) / 100)) + (parseInt((gy2 + 399) / 400)) - 80 + gd + g_d_m[gm - 1];
    jy += 33 * (parseInt(days / 12053));
    days %= 12053;
    jy += 4 * (parseInt(days / 1461));
    days %= 1461;
    if (days > 365) {
        jy += parseInt((days - 1) / 365);
        days = (days - 1) % 365;
    }
    jm = (days < 186) ? 1 + parseInt(days / 31) : 7 + parseInt((days - 186) / 30);
    jd = 1 + ((days < 186) ? (days % 31) : ((days - 186) % 30));
    return [jy, jm, jd];
}