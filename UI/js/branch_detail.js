/**
 * Created by mehran on 12/20/17.
 */

var data_ready = false;
var center_data;
var share_btn_show = false;
$(document).ready(function () {


    // get_details(center_type, center_code);
    // $('#details-options .input-box').val(window.location.href)
    //
    // $('#health-copy').click(function () {
    //     $('#details-options .input-box').select();
    //     document.execCommand("copy")
    //     $.Toast('لینک این صفحه کپی شد', {'duration': 2000, 'class': ''})
    // });
    //
    //
    // $('#options-btn').click(function () {
    //
    //     if (share_btn_show) {
    //         $(this).removeClass('option-rotate');
    //         $('#share-btns').removeClass('show-share-btns');
    //     }
    //     else {
    //         $(this).addClass('option-rotate');
    //         $('#share-btns').addClass('show-share-btns');
    //     }
    //     share_btn_show = !share_btn_show;
    //
    // });
    //
    // $('#health-share').click(function () {
    //
    //     if (share_btn_show) {
    //         $(this).removeClass('option-rotate');
    //         $(this).removeClass('move-health-share');
    //         $('#share-btns').removeClass('show-share-btns');
    //     }
    //     else {
    //         $(this).addClass('option-rotate');
    //         $(this).addClass('move-health-share');
    //         $('#share-btns').addClass('show-share-btns');
    //     }
    //     share_btn_show = !share_btn_show;
    //
    // });


    $(window).scroll(function (evenet) {

        var pos_top = 120;
        var height = $('#options').height();
        var margin = 50;
        var dis = $(window).scrollTop() - $('#footer').offset().top + margin + pos_top + height; //numbers area top and height of sidebar
        if (dis > 0 && $(window).width()>1050 ) {
            var top = pos_top - dis;
            $('#options').css('top', top + 'px');
        }
        else{
            $('#options').css('top', pos_top + 'px');
        }

        // var pos_bottom = 20;
        // margin = 5;
        // dis = $(window).scrollTop() + $(window).height() - $('#footer').offset().top  + margin - pos_bottom; //numbers area top and height of sidebar
        // if (dis > 0 && $(window).width()<1050) {
        //     var bottom = pos_bottom + dis;
        //     $('#options-btn').css('bottom', bottom + 'px');
        // }


    });

    $('.telegram-share').on('click', function () {

        window.open("https://t.me/share/url?url=" + window.location.href);
    });

    $('.twitter-share').on('click', function () {
        window.open("https://twitter.com/share?ref_src=twsrc%5Etfw")
    });

    $('.facebook-share').on('click', function () {

        window.open('https://www.facebook.com/sharer/sharer.php?u=' + window.location.href)
    });

    $('.googleplus-share').on('click', function () {
        window.open('https://plus.google.com/share?url=' + window.location.href);
    });

    $('.linkedin-share').on('click', function () {

        window.open('https://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source=' + window.location.href);
    });

    $('.email-share').on('click', function () {

        window.open('mailto:?' + window.location.href);
    });


});


function get_details(type, code) {

    var URL = url = domain_url + '/api/v1/health_centers/' + type + '/' + code + '/';
    $.ajax({
        url: URL,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", "Negotiate");
        },
        type: "GET",
        crossDomain: true,
        contentType: "application/json;charset=utf-8",
        datatype: "json",
        success: function (data) {
            data_ready = true;
            center_data = data;
            show_detail_data(type);
        },
        error: function (jXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });


}


function show_detail_data(type) {
    var text;
    if (type == 'pharmacy')
        text = center_data['special_name'];
    else
        text = center_data['name'];
    $('#health-header .title2').text(text);

    if (type == 'pharmacy' || type == 'hospital') {

        var content = '';
        content += ' <p class="subtitle">اطلاعات تماس</p>';
        if (center_data['tell']) {
            content += '<p class="hf-text">شماره تلفن</p>';
            content += ' <p class="body-text">' + latin_to_persian( center_data['tell'] ) + '</p>'
        }

        content += '<div class="col-s12"> <div class="col-s6"> <div class="row-v">';
        content += '<p class="hf-text">استان</p>';
        content += ' <p class="body-text">' + center_data['province'] + '</p>';
        content += '</div></div>';
        content += '<div class="col-s6"><div class="row-v"><p class="hf-text">شهر</p>';
        content += '<p class="body-text">' + center_data['city'] + '</p>';
        content += '</div></div></div>';
        var content2 = '';
        var head = '<p class="subtitle">سایر اطلاعات</p>';
        content2 += head;
        if (type == 'hospital') {

            if (center_data['activity']) {
                content2 += '<p class="hf-text">نوع فعالیت</p>';
                var text = '';
                for (var i in center_data['activity']) {
                    if (i != 0)
                        text += ','
                    text += center_data['activity'][i];
                }
                content2 += '<p class="body-text">' + text + '</p>'
            }

            if (center_data['expertise']) {
                content2 += '<p class="hf-text">نوع تخصص</p>';
                content2 += '<p class="body-text">' + center_data['expertise'] + '</p>'
            }

            if (center_data['bed_count']) {
                content2 += '<p class="hf-text">تعداد تخت</p>';
                content2 += '<p class="body-text">' + latin_to_persian(center_data['bed_count'].toString()) + '</p>'
            }

            if (center_data['organization']) {
                content2 += '<p class="hf-text">سازمان متبوع</p>';
                content2 += '<p class="body-text">' + center_data['organization'] + '</p>'
            }

            if (center_data['clinical']) {
                content2 += '<p class="hf-text">خدمات کلینیکال</p>';
                content2 += '<p class="body-text">' + pretty(center_data['clinical']) + '</p>'
            }
            if (center_data['hospitalization']) {
                content2 += '<p class="hf-text">خدمات بیمارستانی</p>';
                content2 += '<p class="body-text">' + pretty(center_data['hospitalization']) + '</p>'
            }
            if (center_data['para_clinic']) {
                content2 += '<p class="hf-text">خدمات پاراکلینیک</p>';
                content2 += '<p class="body-text">' + pretty(center_data['para_clinic']) + '</p>'
            }

            if (center_data['with_star']) {
                content2 += '<p class="hf-text">سایر خدمات</p>';
                content2 += '<p class="body-text">' + pretty(center_data['with_star']) + '</p>'
            }

            if (center_data['site']) {
                content2 += '<p class="hf-text">سایت</p>';
                content2 += '<p class="body-text">' + center_data['site'] + '</p>'
            }

            if (center_data['email']) {
                content2 += '<p class="hf-text">ایمیل</p>';
                content2 += '<p class="body-text">' + center_data['email'] + '</p>'
            }
        }
        else {
            if (center_data['public_name']) {
                content2 += '<p class="hf-text">نام عمومی</p>';
                content2 += '<p class="body-text">' + center_data['public_name'] + '</p>'
            }
            if (center_data['founder']) {
                content2 += '<p class="hf-text">موسس</p>';
                content2 += '<p class="body-text">' + center_data['founder'] + '</p>'
            }
            if (center_data['national_number']) {
                content2 += '<p class="hf-text">شماره ملی</p>';
                content2 += '<p class="body-text">' + latin_to_persian(center_data['national_number']) + '</p>'
            }
            if (center_data['university']) {
                content2 += '<p class="hf-text">دانشگاه</p>';
                content2 += '<p class="body-text">' + center_data['university'] + '</p>'
            }
        }

        if (content2 != head)
            content += content2;
        $('#health-detail').html(content);
    }
    else {
        var content = '';
        content += ' <p class="subtitle">تخصص</p>';

        var text = '';
        for (var i in center_data['profession']) {
            if (i != 0)
                text += ','
            text += center_data['profession'][i];
        }
        content += '<p class="body-text">' + text + '</p>';

        content += ' <p class="subtitle">اطلاعات تماس</p>';
        for (var j in center_data['addresses']) {
            content += '<p class="hf-text">آدرس</p>';
            content += ' <p class="body-text">' + center_data['addresses'][j]['address'] + '</p>';
            content += '<p class="hf-text">شماره تلفن</p>';
            var text = '';
            for (var i in center_data['addresses'][j]['contacts']) {
                if (i != 0)
                    text += ',';
                text += latin_to_persian(center_data['addresses'][j]['contacts'][i]);
            }
            content += '<p style="margin-bottom:40px" class="body-text">' + text + '</p>';
        }

        $('#health-detail').html(content);
    }


}


function map_ready() {
    if (data_ready)
        gmap();
    else
        setTimeout(map_ready, 500);
}

function gmap() {

    if ((center_type == 'pharmacy' || center_type == 'hospital') && center_data['location']['latitude'] != 0) {
        var center = new google.maps.LatLng(center_data['location']['latitude'], center_data['location']['longitude']);
        var mapProp = {
            center: center,
            zoom: 15,
        };
        var map = new google.maps.Map(document.getElementById("map"), mapProp);
        var marker = new google.maps.Marker({
            position: center,
        });
        marker.setMap(map);
    }
    else {
        $('#map').hide();
    }
}

function pretty(text) {

    var result = text.replace(/-/g, "، ");
    return result;

}

function latin_to_persian(num) {

    //num must be string
    var persian_num = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹", '،', ' '];
    var out = "";
    var lim = num.length;
    for (var i = 0; i < lim; i++) {
        var idx = num[i];
        if (idx == ',')
            idx = '10';
        if (idx == ' ')
            idx = '11';
        out = out.concat(persian_num[parseInt(idx)])
    }
    return out
}