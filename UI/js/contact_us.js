
var URL_contact = domain_url + "/api/v1/utility/email/";
var message = {
    origin: "",
    subject: "",
    content: ""
};


$(document).ready(function () {
    function initMap() {
        var center = new google.maps.LatLng('35.700788', '51.354054');
        var mapProp = {
            center: center,
            zoom: 16,
        };

        map = new google.maps.Map(document.getElementById("map"), mapProp);
        var marker = new google.maps.Marker({
            position: center,
        });

        marker.setMap(map);
    }

    $('#submit-message-btn').click(function () {

        var mail = $("#info-input-mail").val();
        var subject = $("#info-input-subject").val();
        var text = $("#info-input-text").val();

        if (mail == "") {

            $.Toast('ابتدا ایمیل خود را وارد کنید', {'duration': 3000, 'class': 'alert'});

        }
        else if (!validateEmail(mail)) {
            $.Toast('لطفا ایمیل خود را به درستی وارد کنید', {'duration': 3000, 'class': 'alert'});

        }
        else if (subject == "") {
            $.Toast('لطفا موضوع پیام خود را وارد کنید', {'duration': 3000, 'class': 'alert'});

        }
        else if (text == "") {
            $.Toast('لطفا متن پیام خود را وارد کنید', {'duration': 3000, 'class': 'alert'});
        }

        else {
            message['origin'] = mail;
            message['subject'] = subject;
            message['content'] = text;
            send_comment();
        }
    });

    $('#telegram').click(function () {
        var telegram = "http://childf.com";
        window.open(telegram);
        ga('send', {
            hitType: 'event',
            eventCategory: 'social',
            eventAction: 'telegram'
        });

        _paq.push(['trackEvent', 'social', 'social', 'telegram']);

    });

    $('#instagram').click(function () {
        var insta = "http://childf.com";
        window.open(insta);
        ga('send', {
            hitType: 'event',
            eventCategory: 'social',
            eventAction: 'instagram'
        });
        _paq.push(['trackEvent', 'social', 'social', 'instagram']);


    });


    $('#aparat').click(function () {
        var aparat = "http://childf.com";
        window.open(aparat);
        ga('send', {
            hitType: 'event',
            eventCategory: 'social',
            eventAction: 'aparat'
        });
        _paq.push(['trackEvent', 'social', 'social', 'aparat']);

    });


});


function send_comment() {
    $.ajax({
        url: URL_contact,
        beforeSend: function (request) {
            request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
        },
        type: "POST",
        datatype: "json",
        data: jQuery.param(message),
        success: function () {
            $.Toast('پیام شما با موفقیت ارسال شد', {'duration': 3000, 'class': ''});
            $("#info-input-mail").val("");
            $("#info-input-name").val("");
            $("#info-input-text").val("");
            $("#info-input-subject").val("");

        },
        error: function (jXHR, textStatus, errorThrown) {
            $.Toast('در ارسال پیام شما با خطا مواجه شدیم. لطفا دوباره تلاش نمایید', {
                'duration': 3000,
                'class': 'alert'
            });

        }
    });


}


function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}