/**
 * Created by mehran on 11/15/17.
 */
$(document).ready(function () {
    $('#tos-link').click(function () {
        window.location.assign(domain_url + '/tos/')
    });


    $('#panel-link').click(function () {
        window.location.assign(domain_url + '/physician/')
    });


    $('#telegram').click(function () {
        var telegram = "https://t.me/darmaneh";
        window.open(telegram);
        // ga('send', {
        //     hitType: 'event',
        //     eventCategory: 'social',
        //     eventAction: 'telegram'
        // });

        _paq.push(['trackEvent', 'social', 'social', 'telegram']);

    });

    $('#insta').click(function () {
        var insta = "https://www.instagram.com/darmaneh/?hl=en";
        window.open(insta)
        // ga('send', {
        //     hitType: 'event',
        //     eventCategory: 'social',
        //     eventAction: 'instagram'
        // });
        _paq.push(['trackEvent', 'social', 'social', 'instagram']);


    });


    $('#aparat').click(function () {
        var aparat = " http://www.aparat.com/darmaneh";
        window.open(aparat)
        // ga('send', {
        //     hitType: 'event',
        //     eventCategory: 'social',
        //     eventAction: 'aparat'
        // });
        _paq.push(['trackEvent', 'social', 'social', 'aparat']);

    });


    $('.link-iran').click(function () {
        window.open('https://taps.io/Bzjeg')
    })

    $('.link-bazar').click(function () {
        window.open('https://taps.io/Bwqig')
    });
    $('.link-play').click(function () {
        window.open('https://taps.io/Bwqiw')
    });
    $('.link-sib').click(function () {
        window.open('https://taps.io/darmanehIOS')
    });


    $('#send-link-btn').click(function () {
        send_app_link();
    });

    $('#comming-btn').click(function () {
        store_number();
    })

});