var done = true;
var book = true;
var last_item;
jQuery.fn.exists = function () {
    return this.length > 0;
}

function handle_book() {

    var func = 'add';
    if (book)
        func = 'remove';

    $.ajax({
        url: domain_url + "/api/v1/user/patient/condition/",
        beforeSend: function (request) {
            request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
            request.setRequestHeader("Authorization", "TOKEN " + $.cookie("token"));
            request.withCredentials = true;

        },
        type: "POST",
        contentType: "application/x-www-form-urlencoded",
        datatype: "json",
        data: jQuery.param({
            func: func,
            id: id
        }),

        success: function (data) {

            if (book) {
                book = false;
                document.getElementById('bookmark').innerHTML = "حذف شد";
                bookmark_done();
                setTimeout(bookmark_remove, 1500);

            }
            else {
                book = true;

                document.getElementById('bookmark').innerHTML = "اضافه شد";
                bookmark_done();
                setTimeout(bookmark_add, 1500);

            }

        },
        error: function (jXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}

function bookmark_done() {
    $('#bookmark').css('background-color', 'var(--green)').css('border', 'none').css('color', 'white').css('background-image', "url(" + static_url + "/img/Icon/ic_done_24px.svg)").css('padding-top', '7px');

}
function bookmark_add() {
    $('#bookmark').css('background-color', 'var(--darmaneh-blue)').css('background-image', "url(" + static_url + "/img/Icon/ic_bookmark_24px.svg)").css('color', 'white').css('padding-top', '7px');
    document.getElementById('bookmark').innerHTML = "حذف"
}

function bookmark_remove() {
    $('#bookmark').css('background-color', 'white').css('border', 'var(--darmaneh-blue) solid 2px').css('color', 'var(--darmaneh-blue').css('background-image', "url(" + static_url + "/img/Icon/ic_bookmark_blue_24px.svg)").css('padding-top', '5px');
    ;
    document.getElementById('bookmark').innerHTML = "منتخب";
}

var animateEnds;

$(window).ready(function () {


    if ($(this).width() <= 1300) {
        $('.share-buttons-mobile-container').show();
        $('.share-buttons-mobile').show();
    }

    else {
        $('.share-buttons-mobile-container').hide();
        $('.share-buttons-mobile').hide();
    }
    find_last_element();

    animateEnds = true;

    $('#page_link').val(window.location.href);

    if ($('#video').exists()) {
        $('#video-item').css('color', 'var(--darmaneh-blue)');
    }
    else {
        $('#description-item').css('color', 'var(--darmaneh-blue)');
    }


    if (book_state == 'False')
        book = false;

    if (book) {
        bookmark_remove();
    }

    $('#bookmark').on('click', function () {
        if ($.cookie("logged_in") == 1) {
            handle_book();
        }

        else {

            $.Toast('ابتدا وارد حساب کاربری خود شوید', {'duration': 3000, 'class': 'alert'});
        }
    });


    var share_buttons_state = 0;
    var share_buttons_state_mobile = 0;
    var hamburger_menu_state = 0;
    $('.share-button').on('click', function () {
        share_buttons_state++;
        if (share_buttons_state % 2 == 1) {
            $('button.share-button').toggleClass('share-button-transition');
            $('div.share_buttons-container').show(700);
            $(this).css('background-image', "url(" + static_url + "/img/Icon/ic_close_24px.svg)");
            $(this).css('background-size', '8px');
            $(this).css('background-position', '50% 50% ');
        }
        else {
            $('button.share-button').toggleClass('share-button-transition');
            $('div.share_buttons-container').hide(700);
            $(this).css('background-image', "url(" + static_url + "/img/Icon/ic_share_24px.svg)");
            $(this).css('background-size', '50%');
            $(this).css('background-position', '45% 50%');
        }
    });

    $('.share-button-mobile').on('click', function () {
        share_buttons_state_mobile++;
        if (share_buttons_state_mobile % 2 == 1) {
            $('button.share-button-mobile').toggleClass('share-button-mobile-transition');
            $(this).css('background-image', "url(" + static_url + "/img/Icon/ic_close_24px.svg)");
            $(this).css('background-size', '25px');
            $(this).css('background-position', '50% 50% ');
            $('.share-buttons-mobile-container').toggleClass('expand-share-buttons-container-height');
        }
        else {
            $('button.share-button-mobile').toggleClass('share-button-mobile-transition');
            $(this).css('background-image', "url(" + static_url + "/img/Icon/ic_share_24px.svg)");
            $(this).css('background-size', '25px');
            $(this).css('background-position', '45% 50% ');
            $('.share-buttons-mobile-container').toggleClass('expand-share-buttons-container-height');


        }
    });

    $('.hamburger-navbar').on('click', function () {
        hamburger_menu_state++;
        if (hamburger_menu_state % 2 == 1) {
            $(this).toggleClass('hamburger-navbar-click').css('background-image', "url(" + static_url + "/img/Icon/ic_close_menu_black.svg)");
        }


        else {
            $(this).toggleClass('hamburger-navbar-click').css('background-image', "url(" + static_url + "/img/Icon/ic_nav_menu_black.svg)");
        }
    });


    $('.page-items-title').on('click', function (event) {
        event.preventDefault();
        animateEnds = false;
        $('li.page-items-title').css('color', 'var(--gray)');
        var title = $(this).attr('target');
        var hash = '#' + title;

        if (hash == '#video') {
            $(' html , body ').animate({scrollTop: $(hash).offset().top - 60}, 'slow', function () {
                animateEnds = true;
                handleScroll($(this).attr('id'));
            });
        }

        else {
            $(' html , body ').animate({scrollTop: $(hash).offset().top - 40}, 'slow', function () {
                animateEnds = true;
                handleScroll($(this).attr('id'));
            });
        }
    });


    $('.telegram-button').on('click', function () {

        window.open("https://t.me/share/url?url=" + window.location.href);
    });

    $('.tweeter-button').on('click', function () {
        window.open("https://twitter.com/share?ref_src=twsrc%5Etfw")
    });

    $('.facebook-button').on('click', function () {

        window.open('https://www.facebook.com/sharer/sharer.php?u=' + window.location.href)
    });

    $('.google-plus-button').on('click', function () {
        window.open('https://plus.google.com/share?url=' + window.location.href);
    });

    $('.in-button').on('click', function () {

        window.open('https://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source=' + window.location.href);
    });

    $('.mail-button').on('click', function () {

        window.open('mailto:?' + window.location.href);
    });

    $('.copy-share-link').on('click', function () {

        $('#page_link').select();
        document.execCommand("Copy");


    });


    $(window).scroll(function (event) {
        event.preventDefault();
        if (animateEnds) {
            handleScroll();
        }

        if ($(this).scrollTop() < 95) {
            $('.items').css('margin-top', 110 - $(this).scrollTop());
        }

        else {
            $('.items').css('margin-top', '20px');
        }


        if ($('.share-container').offset().top + $('.items').height() + 20 + $('.share-page').height() + 30 > $(document).height() - 400) {

            $('.items').css('margin-top', -($('.share-container').offset().top + $('.items').height() + 20 + $('.share-page').height() + 30 - ($(document).height() - 400)));
        }


    })
});


function find_last_element() {
    var itemArray = ['#video', '#description', '#cause', '#symptom', '#diagnosis', '#treatment', '#preventing', '#risk_factor', '#visit_doctor', '#ask_doctor', '#expectation', '#got_worse', '#other'];


    for (var i in itemArray) {
        if ($(itemArray[i]).exists()) {
            last_item = itemArray[i];
        }
    }
}


function handleScroll() {

    var itemArray = ['#video', '#description', '#cause', '#symptom', '#diagnosis', '#treatment', '#preventing', '#risk_factor', '#visit_doctor', '#ask_doctor', '#expectation', '#got_worse', '#other'];
    var itemArrayId = ['#video-item', '#description-item', '#cause-item', '#symptom-item', '#diagnosis-item', '#treatment-item', '#preventing-item', '#risk-factor-item', '#visit-doctor-item', '#ask-doctor-item', '#expectation-item', '#got-worse-item', '#other-item'];

    for (var item in itemArray) {

        if ($(itemArray[item]).exists()) {

            if (parseInt($(window).scrollTop()) >= parseInt($(itemArray[item]).offset().top - 60)) {
                $('li.page-items-title').css('color', 'var(--gray)');
                $(itemArrayId[item]).css('color', 'var(--darmaneh-blue)');
            }


        }
    }
}