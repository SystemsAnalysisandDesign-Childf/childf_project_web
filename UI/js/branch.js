/**
 * Created by mehran on 6/24/17.
 */
var URL_health = domain_url + "/health_centers/";
var URL_api_health = domain_url + "/api/v1/health_centers/province_city/";
var data_received;
var prov_list = [];
var city_list = [];
var city_list_loaded = false;

$(document).ready(function () {

    var content=' <option hidden>شهر</option>';
    $('#city').html(content);
    content = ' <option hidden>استان</option>';
    $('#prov').html(content);

    $('#admit-btn').click(function () {
        redirect();
    });

    if (!city_list_loaded) {
        $.ajax({
            url: URL_api_health,
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Negotiate");
            },
            type: "GET",
            crossDomain: true,
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            success: function (data) {
                data_received = data;
                city_list_loaded = true;
                var option = '';
                for (obj in data) {
                    prov_list.push(data[obj]['name']);
                    option = '<option>'+data[obj]['name']+'</option>';
                    $('#prov').append(option)

                }
            },
            error: function (jXHR, textStatus, errorThrown) {
            }
        });
    }


    $('#prov').on('change', function () {

        // 1 based index in drop down
        var index = $(this)[0].selectedIndex;
        index= index - 1;
        var temp = data_received[index]['cities'];
        var i;
        var content=' <option hidden>شهر را انتخاب کنید</option>';
        for (i in temp)
        {
            content+='<option>'+temp[i]+'</option>';

        }
         $('#city').html(content);
    })

});

 // $.Toast('یکی از گزینه ها را انتخاب کنید', {'duration': 2000, 'class': 'alert'});

// function fade_alert(text) {
//     $('#alert-text').text(text);
//     $('#alert-text').fadeIn(500);
//     setTimeout(
//         function () {
//             $('#alert-text').fadeOut(500);
//
//         }, 1500);
// }

function redirect() {
    var prov_index=$('#prov')[0].selectedIndex -1 ;
    var city_index=$('#city')[0].selectedIndex -1 ;
    if (prov_index ==  -1) {
         $.Toast('ابتدا استان را انتخاب کنید', {'duration': 2000, 'class': 'alert'});
    }
    else if (city_index == -1) {
         $.Toast('شهر را انتخاب کنید', {'duration': 2000, 'class': 'alert'});
     }
    else {
       window.location.assign(URL_health + "province/" + prov_list[prov_index] + "/city/" + data_received[prov_index]['cities'][city_index]);

     }
}


