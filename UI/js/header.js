/**
 * Created by mehran on 11/15/17.
 */

var URL = domain_url + "/content_pool/condition_details/";
var URL_api = domain_url + "/api/v1/content_pool/condition_details/";
var user_input;
var disease_list_loaded = false;
var disease_list = [];
var code_list = [];

$(document).ready(function () {

    //nav scroll function
    $('.nav-anchor').click(function (event) {
        event.preventDefault();
        var title = $(this).attr('target');
        var hash = '#' + title;

        if (page_type == 'main') {
            _paq.push(['setDocumentTitle', title]);
            _paq.push(['trackPageView']);
            $(' html , body ').animate({scrollTop: $(hash).offset().top}, 'slow', function () {
                window.location.hash = hash;
            });
        }
        else {
            window.location.assign(domain_url + '/' + hash);
        }
    });

    var mobileNavBarTurn = 0;
    $('.navBarMobileModal').on('click', function (event) {
        $(this).fadeOut(500);
        mobileNavBarTurn = 0;
    });

    $('.navBarMobileButton').on('click', function (event) {
        event.preventDefault();
        mobileNavBarTurn++;
        if (mobileNavBarTurn % 2 == 1) {
            $('.navBarMobileModal').fadeIn(500);
        }
        else {
            $('.navBarMobileModal').fadeOut(500);
            mobileNavBarTurn = 0;
        }
    });

    $('#close').on('click', function (event) {
        event.preventDefault();

    });

    //search bar:
    $('.search-in').val('');

    $(".search-in").on('keyup', function (e) {
        if (e.keyCode == 13) {
            redirect_to_details_head();
        }
    });


    if (!disease_list_loaded) {
        $.ajax({
            url: URL_api,
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", "Negotiate");
            },
            type: "GET",
            crossDomain: true,
            contentType: "application/json;charset=utf-8",
            datatype: "json",
            success: function (data) {
                disease_list_loaded = true;
                for (obj in data) {
                    disease_list.push(data[obj]['name']);
                    code_list.push(data[obj]['id'])
                }


                $(".search-in").autocomplete({
                    source: disease_list,
                    select: function (event, ui) {
                        $('.search-in').val(ui.item.value)
                        redirect_to_details_head()
                        // window.location.assign(URL + ui.item.value);
                    },
                    open: function (event, ui) {
                    },
                    close: function (event, ui) {
                    }
                });


            },
            error: function (jXHR, textStatus, errorThrown) {
            }
        });
    }


});


function redirect_to_details_head() {
    user_input = $('.search-in').val();
    var idx = disease_list.indexOf(user_input)
    var id = "000";

    if (idx != -1 && user_input != "") {
        id = code_list[idx];
        window.location.assign(URL + id + '/' + user_input);
    }
    if (idx == -1) {
        $.Toast('این مورد در لیست بیماری ها نیست', {'duration': 2000, 'class': 'alert'});
    }
}
