var request_url = global_url + '/api';

$(window).ready(function () {

    $('#name').fadeIn(1000);
    $('#lname').fadeIn(1200);
    $('#profId').fadeIn(1400);
    $('#profession').fadeIn(1600);
    $('#professionCategory').fadeIn(1800);
    $('#addCategory').fadeIn(1800);
    $('#categoryList').fadeIn(1800);
    $('#phone').fadeIn(2000);
    $('#mail').fadeIn(2200);
    $('#pass').fadeIn(2400);
    $('#eye').fadeIn(2600);
    $('#Repass').fadeIn(2800);
    $('#register').fadeIn(3000);
    $('#login').fadeIn(3200);

    var data = ['همیار','مددجو','مددکار','مدیر میانی'];
    sign_up_profession_data = data;
    var profession = document.getElementById('profession');
    for (i in data) {

        var option = document.createElement('option');
        option.innerHTML = data[i];
        option.style.color = "#93B6C7";
        profession.appendChild(option);
    }

});
// function getProfessionCategories() {
//     var profession = ($('#profession').val());
//     var professionCategory = document.getElementById('professionCategory');
//     $('#professionCategory option').remove();
//     for (i in sign_up_profession_data) {
//         if (profession == sign_up_profession_data[i]) {
//             categoryId = i;
//             for (j in sign_up_profession_data[i]) {
//                 var category = document.createElement('option');
//                 category.innerHTML = sign_up_profession_data[i];
//                 category.style.color = "#93B6C7";
//                 professionCategory.appendChild(category);
//             }
//         }
//         else {
//             continue;
//         }
//     }
// }
// function addCategory() {
//     var list = document.getElementById('addedCategories');
//     var id;
//     var categoryName = $('#professionCategory').val();
//     for (j in sign_up_profession_data[categoryId].professions) {
//         if (categoryName == sign_up_profession_data[categoryId].professions[j].name) {
//             id = sign_up_profession_data[categoryId].professions[j].id;
//             if (categoryList.includes(id)) {
//                 $('#professionCategory').css('border', '#FC6F6F solid 2px')
//             }
//             else {
//                 categoryList.push(id);
//                 var listCategory = document.createElement('div');
//                 var exit = document.createElement('button');
//                 listCategory.className = 'addedCategoriesText col-md-12';
//                 listCategory.id = id;
//                 exit.className = 'exit';
//                 exit.id = id;
//                 exit.innerHTML = '&times;';
//                 listCategory.innerText = categoryName;
//                 listCategory.appendChild(exit);
//                 list.appendChild(listCategory);
//                 $('#addCategory').css('background-color', '#24B337');
//                 $('#addCategory').css('background-image', "url(" + static_url + "/img/icons/check-mark2.png)");
//             }
//         }
//         else
//             continue;
//     }
// }
// var codeEntered = 0;
// function sendVerificationCode(phone_number, pass) {
//     $('#header').fadeOut();
//     $('#regForm').fadeOut();
//     $('#header2').fadeIn(1200);
//     $('#verifyCode').fadeIn(1500);
//     $('#verifyText').fadeIn(1800);
//     $('#verifyButton').fadeIn(2100);
//     var timeleft = 60;
//     var downloadTimer = setInterval(function () {
//         timeleft--;
//         document.getElementById("countDown").textContent = " " + timeleft + " ";
//         if (timeleft == 0 && codeEntered == 0) {
//             $('#callMe').show(1000);
//             $('#verifyText').hide(1000);
//         }
//         else if (codeEntered == 1) {
//             $('#verifyText').hide(1000);
//         }
//         else if (timeleft <= 0 && codeEntered == 0)
//             clearInterval(downloadTimer);
//     }, 1000);
//     $('#callMe').on('click', function (event) {
//         var text = document.getElementById("verifyText");
//         var callText = document.getElementById("voiceVerifyText");
//         var voiceCountDown = document.getElementById('countDown2');
//         $('#verifyText').hide(1000);
//         event.preventDefault();
//         var calling = document.getElementById('callMe');
//         $('#voiceVerifyingText').hide(1000);
//         $('#callMe').css("background-image", "none");
//         calling.innerHTML = "...";
//         $.ajax({
//             url: request_url + "/v1/physician/send_verification/",
//             type: "POST",
//             beforeSend: function (request) {
//                 request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
//             },
//             data: {phone_number: phone_number, protocol: 'voice'},
//             success: function (data) {
//                 calling.innerHTML = "تماس";
//                 $('#callingError').hide();
//                 $('#callMe').hide();
//                 $('#voiceVerifyingText').show(1000);
//                 var timeleft = 60;
//                 var downloadTimer = setInterval(function () {
//                     timeleft--;
//                     document.getElementById("countDown2").textContent = " " + timeleft + " ";
//                     if (timeleft == 0 && codeEntered == 0) {
//                         $('#voiceVerifyingText').hide(1000);
//                         $('#callMe').show(1000);
//                     }
//                     else if (codeEntered == 1) {
//                         $('#voiceVerifyingText').hide(1000);
//                     }
//                     else if (timeleft <= 0 && codeEntered == 0) {
//                         clearInterval(downloadTimer);
//                     }
//                 }, 1000);
//             },
//             error: function (jXHR, textStatus, errorThrown) {
//                 calling.innerHTML = "تماس";
//                 $('#callMe').css("background-image", "url(" + static_url + "/css/img/icons/refresh-button.png)");
//                 $('#callMe').css("background-repeat", "no-repeat");
//                 $('#callMe').css("background-position", "35% 50%");
//                 $('#calling').hide();
//                 $('#callingError').fadeIn(1000);
//             }
//         })
//     });
//     $.ajax({
//         url: request_url + "/v1/physician/send_verification/",
//         type: "POST",
//         beforeSend: function (request) {
//             request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
//         },
//         data: {phone_number: phone_number, protocol: 'sms'},
//         success: function (data) {
//         },
//         error: function (jXHR, textStatus, errorThrown) {
//         }
//     });
//     $('#verifyButton').on('click', function (event) {
//         event.preventDefault();
//         var verify_code;
//         verify_code = document.getElementById('verifyCode').value;
//         $('#calling').hide();
//         $('#callingError').hide();
//         if (verify_code.length == 6) {
//             $.ajax({
//                 url: request_url + "/v1/physician/set_change_pass/",
//                 type: "POST",
//                 dataType: 'json',
//                 contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
//                 beforeSend: function (request) {
//                     request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
//                 },
//                 data: {phone_number: phone_number, password: pass, verify_code: verify_code},
//                 success: function (data) {
//                     codeEntered = 1;
//                     var verifyButtonText = document.getElementById('verifyButton');
//                     verifyButtonText.innerHTML = "";
//                     $('#incorrect').fadeOut(1000);
//                     $('#expire').fadeOut(1000);
//                     $('#empty').fadeOut(1000);
//                     $('#verifyButton').css('background-color', "#00cc66");
//                     $('#verifyButton').css("background-repeat", "no-repeat");
//                     $('#verifyButton').css('background-image', "url(" + static_url + "/img/icons/check-mark.png)");
//                     $('#verifyButton').css('background-position', "50% 50%");
//                     $('#verifyText').hide(1000);
//                     $('#voiceVerifyingText').hide(1000);
//                     $('#verifyCode').hide();
//                     $('#callMe').hide();
//                     $('#success').fadeIn(1200);
//                 },
//                 error: function (jXHR, textStatus, errorThrown) {
//                     var response = JSON.parse(jXHR.responseText)
//                     if (response.detail === 'verify_code mismatch') {
//                         $('#expire').hide();
//                         $('#empty').hide();
//                         $('#incorrect').fadeIn(1000);
//                     }
//                     else if (response.detail === 'verify_code has been expired') {
//                         $('#incorrect').hide();
//                         $('#empty').hide();
//                         $('#expire').fadeIn(1000);
//                     }
//                     else if (response.detail === "verify_code must be 6 characters") {
//                         $('#expire').hide();
//                         $('#incorrect').hide();
//                         $('#empty').fadeIn(1000);
//                     }
//                 }
//             });
//         }
//         else {
//             $('#verifyCode').css('border', '#FC6F6F solid 2px');
//             $('#codeLength').show(1000);
//         }
//     });
// }
// function sendData() {
//     var prof_id;
//     var phone_number;
//     var first_name;
//     var last_name;
//     var email;
//     var pass;
//     var phone_validate;
//     prof_id = document.getElementById("profId").value;
//     phone_number = document.getElementById("phone").value;
//     first_name = document.getElementById("name").value;
//     last_name = document.getElementById("lname").value;
//     email = document.getElementById("mail").value;
//     pass = document.getElementById("pass").value;
//     phone_validate = Number(phone_number);
//     var id_validate = Number(prof_id);
//
//
//     if (first_name === "") {
//         $('#name').css('border', '#FC6F6F solid 2px');
//     }
//     else if (last_name === "") {
//         $('#lname').css('border', '#FC6F6F solid 2px');
//     }
//     else if ((prof_id === "")) {
//         $('#profId').css('border', '#FC6F6F solid 2px');
//
//     }
//     else if (isNaN(prof_id)) {
//         $('#profId').css('border', '#FC6F6F solid 2px');
//     }
//     else if (id_validate === "NaN") {
//         $('#profId').css('border', '#FC6F6F solid 2px');
//     }
//     else if ((phone_number.length != "11") || phone_number.substring(0, 2) != "09" || phone_validate == "NaN") {
//         $('#phone').css('border', '#FC6F6F solid 2px');
//     }
//     else if (pass === "" || pass.length <= "6") {
//         $('#pass').css('border', '#FC6F6F solid 2px');
//         $('#pass').innerText = "";
//         $('#pass').css('font-size', '1.1vw');
//         $('#pass').attr("placeholder", "گذرواژه باید بیش از ۶ حرف باشد")
//     }
//
//
//     else {
//
//         var data = {
//             'prof_id': prof_id,
//             'phone_number': phone_number,
//             'first_name': first_name,
//             'last_name': last_name,
//             'email': email,
//             'profession': []
//         };
//
//         $.extend(data.profession, categoryList);
//
//         $.ajax({
//             url: request_url + "/v1/physician/sign_up/",
//             type: "POST",
//             datatype: "json",
//             contentType: "application/json;charset=utf-8",
//             beforeSend: function (request) {
//                 request.setRequestHeader("X-CSRFTOKEN", $.cookie("csrftoken"));
//             },
//             data: JSON.stringify(data),
//
//             success: function () {
//                 sendVerificationCode(phone_number, pass);
//             },
//             error: function (jXHR, textStatus, errorThrown) {
//             }
//         })
//     }
// }
$('#register').on('click', function (event) {
    event.preventDefault();
    sendData();
});
$('#login').on('click', function (event) {
    event.preventDefault();
    window.location.href = global_url + "/physician/login/"
});
$('#name').focusin(function () {

    $(this).css("border", "white solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/user.png)");
});

$('#name').focusout(function () {

    $(this).css("border", "#93B6C7  solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/user_50.png)");
});
$('#lname').focusin(function () {

    $(this).css("border", "white solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/user.png)");
});

$('#lname').focusout(function () {

    $(this).css("border", "#93B6C7  solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/user_50.png)");
});
$('#profId').focusin(function () {

    $(this).css("border", "white solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/id.png)");
});

$('#profId').focusout(function () {

    $(this).css("border", "#93B6C7  solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/id_50.png)");
});

$('#profession').focusin(function (event) {

    $('#professionCategory').css('border', '#93B6C7 solid 2px');
    event.preventDefault();
    $(this).css("border", "white solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/expertise.png)");


})
$('#profession').focusout(function () {
    $(this).css("border", "#93B6C7 solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/expertise_50.png)");
    getProfessionCategories();

})

$('#phone').focusin(function () {

    $(this).css("border", "white solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/phone.png)");
});

$('#phone').focusout(function () {

    $(this).css("border", "#93B6C7  solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/phone_50.png)");
});

$('#mail').focusin(function () {

    $(this).css("border", "white solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/email.png)");
});

$('#mail').focusout(function () {

    $(this).css("border", "#93B6C7  solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/email_50.png)");
});

$('#pass').focusin(function () {

    $(this).css("border", "white solid 2px");
    $('#eye').css("border", "white solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/pass.png)");
});

$('#pass').focusout(function () {

    $(this).css("border", "#93B6C7  solid 2px");
    $('#eye').css("border", "#93B6C7  solid 2px");
    $(this).css("background-image", "url(" + static_url + "/img/icons/pass_50.png)");
});

$('#eye').mouseover(function () {
    $('#pass').attr('type', 'text');
    $('#pass').css('font-size', '1.1vw');
});
$('#eye').mouseleave(function () {
    var pass = document.getElementById('pass').value;
    $('#pass').attr('type', 'password');

    if (pass.length != "") {
        $('#pass').css('font-size', '0.8vw');
    }
    else {
        $('#pass').css('font-size', '1.1vw');
    }
});


$('#verifyCode').focusin(function () {
    $(this).css("border", "white solid 2px");
});
$('#verifyCode').focusout(function () {
    $(this).css("border", "#93B6C7 solid 2px");
});

$('#addCategory').on('click', function (event) {
    event.preventDefault();
    $('#professionCategory').css('border', '#93B6C7 solid 2px');
    addCategory();
});
$('#addCategory').focusout(function () {
    $(this).css('background-color', '#93B6C7');
    $(this).css('background-image', "url(" + static_url + "/img/icons/add.png)");
});
$('#professionCategory').focusin(function () {
    $('#professionCategory').css('border', '#93B6C7 solid 2px');
});
$('#professionCategory').focusout(function () {
    $('#professionCategory').css('border', '#93B6C7 solid 2px');
});
$('#categoryList').on('click', function (event) {
    event.preventDefault();
    $('#listPopUp').show();
});


$(document).on('click', '.exit', function () {
    var id = $(this).attr('id');
    for (i in categoryList) {

        if (categoryList[i] == id)
            categoryList.splice(i, 1);

        else
            continue;
    }
    $(this).parent().remove();
});

$('#submitChanges').on('click', function () {

    $('#listPopUp').hide();
});


