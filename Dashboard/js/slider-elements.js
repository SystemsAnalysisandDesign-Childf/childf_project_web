﻿//Sliders
		$( "#slider" ).slider();

		$( "#master" ).slider({
	      value: 60,
	      orientation: "horizontal",
	      range: "min",
	      animate: true
	    });

		$( "#slider-range" ).slider({
	      range: true,
	      min: 0,
	      max: 500,
	      values: [ 75, 300 ],
	      slide: function( event, ui ) {
	        $( "#amount-range" ).html( "بازه قیمت: " + ui.values[ 0 ] + " هزار تومان -" + ui.values[ 1 ] + " هزار تومان" );
	      }
	    });
	    $( "#amount-range" ).html( "بازه قیمت: " + $( "#slider-range" ).slider( "values", 0 ) +
		   " هزار تومان -" + $( "#slider-range" ).slider( "values", 1 ) + " هزار تومان" );


	    $( "#slider-range-min" ).slider({
	      range: "min",
	      value: 37,
	      min: 1,
	      max: 700,
	      slide: function( event, ui ) {
	        $( "#amount-range-min" ).html( "حداقل قیمت: " + ui.value + " هزار تومان" );
	      }
	    });
	    $( "#amount-range-min" ).html( "حداقل قیمت: " + $( "#slider-range-min" ).slider( "value" ) + " هزار تومان" );

	    $( "#slider-range-max" ).slider({
	      range: "max",
	      min: 1,
	      max: 10,
	      value: 2,
	      slide: function( event, ui ) {
	        $( "#amount-range-max" ).html( "حداکثر قیمت: " + ui.value + " هزار تومان" );
	      }
	    });
	    $( "#amount-range-max" ).html( "حداکثر قیمت: " + $( "#slider-range-max" ).slider( "value" ) + " هزار تومان" );

	    $( "#steps-slider" ).slider({
	      value:100,
	      min: 0,
	      max: 500,
	      step: 50,
	      slide: function( event, ui ) {
	        $( "#amount-steps-slider" ).html( "مبلغ به تومان: " + ui.value + " هزار تومان" );
	      }
	    });
	    $( "#amount-steps-slider" ).html( "مبلغ به تومان: " + $( "#steps-slider" ).slider( "value" ) + " هزار تومان" );

	    $( "#slider-vertical" ).slider({
	      orientation: "vertical",
	      range: "min",
	      min: 0,
	      max: 100,
	      value: 60,
	      slide: function( event, ui ) {
	        $( "#amount-slider-vertical" ).html("بلندی صدا: " + ui.value );
	      }
	    });
	    $( "#amount-slider-vertical" ).html( "بلندی صدا: " + $( "#slider-vertical" ).slider( "value" ) );

	    $( "#slider-vertical-range" ).slider({
	      orientation: "vertical",
	      range: true,
	      values: [ 17, 67 ],
	      slide: function( event, ui ) {
	        $( "#amount-slider-vertical-range" ).html( "قیمت از: " + ui.values[ 0 ] + " هزار تومان - " + ui.values[ 1 ] + " هزار تومان" );
	      }
	    });
	    $( "#amount-slider-vertical-range" ).html( "قیمت از: " + $( "#slider-vertical-range" ).slider( "values", 0 ) +
	      " هزار تومان - " + $( "#slider-vertical-range" ).slider( "values", 1 ) + " هزار تومان" );

		  $( "#eq > span" ).each(function() {
	      // read initial values from markup and remove that
	      var value = parseInt( $( this ).text(), 10 );
	      $( this ).empty().slider({
	        value: value,
	        range: "min",
	        animate: true,
	        orientation: "vertical"
	      });
	    });