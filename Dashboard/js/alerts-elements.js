﻿ $("#alert").on( 'click', function () {
			alertify.alert("این یک پنجره ی هشدار می باشد");
			return false;
		});

		$("#confirm").on( 'click', function () {
			alertify.confirm("این یک پنجره تایید می باشد", function (e) {
				if (e) {
					alertify.success("آیتم مورد نظر را تایید کردید");
				} else {
					alertify.error("شما دکمه انصراف را کلیک کردید");
				}
			});
			return false;
		});

		$("#prompt").on( 'click', function () {
			alertify.prompt("این پنجره پرامپت می باشد", function (e, str) {
				if (e) {
					alertify.success("شما دکمه تایید را کلیک کردید و نوشتید: " + str);
				} else {
					alertify.error("شما دکمه انصراف را کلیک کردید");
				}
			}, "داده ی پیش فرض");
			return false;
		});

		$("#custom").on( 'click', function () {
			alertify.set({ labels: { ok: "بله", cancel: "خیر" } });
			alertify.confirm("آیا این قالب را دوست دارید؟", function (e) {
				if (e) {
					alertify.success("نظر لطف شماست");
				} else {
					alertify.error("از اینکه این قالب باب میلتان نیست ناراحتیم");
				}
			});
			return false;
		});


		//Messenger
		/*
			This should be used as default outside.

			Messenger.options = {
				extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
			    theme: 'flat'
			}

		*/

		$('#simpleMessenger').on('click',function(){
			//Note this is used for theme and can be used globally.
			Messenger.options = {
				extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
			    theme: 'flat'
			}
			Messenger().post("این تنها یک نوتیفیکیشن ساده می باشد");
		});
		$('#closebuttonMessenger').on('click',function(){

			//Note this is used for theme and can be used globally.
			Messenger.options = {
				extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
			    theme: 'flat'
			}

			Messenger().post({
			  message: 'خطایی رخ داده است',
			  type: 'error',
			  showCloseButton: true,
			});
		});

		$('#TopMessenger').on('click',function(){
			//Note this is used for theme and can be used globally.
			Messenger.options = {
				extraClasses: 'messenger-fixed messenger-on-top',
			    theme: 'flat'
			}
			Messenger().post("این تنها یک نوتیفیکیشن است");
		});

		$('#TopleftMessenger').on('click',function(){
			//Note this is used for theme and can be used globally.
			Messenger.options = {
				extraClasses: 'messenger-fixed messenger-on-top messenger-on-left',
			    theme: 'flat'
			}
			Messenger().post("این تنها یک نوتیفیکیشن است");
		});

		$('#ToprightMessenger').on('click',function(){
			//Note this is used for theme and can be used globally.
			Messenger.options = {
				extraClasses: 'messenger-fixed messenger-on-top messenger-on-right',
			    theme: 'flat'
			}
			Messenger().post("این تنها یک نوتیفیکیشن است");
		});

		$('#BottomMessenger').on('click',function(){
			//Note this is used for theme and can be used globally.
			Messenger.options = {
				extraClasses: 'messenger-fixed messenger-on-bottom',
			    theme: 'flat'
			}
			Messenger().post("این تنها یک نوتیفیکیشن است");
		});

		$('#BottomleftMessenger').on('click',function(){
			//Note this is used for theme and can be used globally.
			Messenger.options = {
				extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-left',
			    theme: 'flat'
			}
			Messenger().post("این تنها یک نوتیفیکیشن است");
		});